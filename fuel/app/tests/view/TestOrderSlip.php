<?php

/**
 * @group Hamee2
 */

use Facebook\WebDriver\Exception\NoSuchWindowException;
use Facebook\WebDriver\Remote\DesiredCapabilities;
use Facebook\WebDriver\Remote\RemoteWebDriver;
use Facebook\WebDriver\WebDriverBy;
use Facebook\WebDriver\WebDriverExpectedCondition;
use Facebook\WebDriver\WebDriverKeys;
use Facebook\WebDriver\WebDriverSelect;
use PHPUnit\Framework\TestCase;
use TestView\LoginDataProvider;

class TestOrderSlip extends TestCase
{

    /**
     * @var RemoteWebDriver
     */
    protected $webDriver;

    /**
     * @var DesiredCapabilities
     */
    protected $capabilities;
    protected $url               = 'https://vm-ne-dev/UserjyuchuIkkatu';
    protected $serverURL         = 'http://localhost:4444/wd/hub';
    protected $connectionTimeout = 60000;
    protected $requestTimeout    = 60000;
    protected $timeWaitAlert     = 20;

    protected function setUp()
    {

        $this->capabilities = DesiredCapabilities::chrome();
        $this->webDriver    = RemoteWebDriver::create(
            $this->serverURL,
            $this->capabilities,
            $this->connectionTimeout,
            $this->requestTimeout
        );

        $this->_login();
    }

    /**
     * @dataProvider getItemDataProvider
     */
    public function testItemUpdate(array $inputs = array())
    {
        try {
            $this->_selectItem($inputs);
            $this->assertEquals(
                $this->url . '/search',
                $this->webDriver->getCurrentURL()
            );

            $blocks = $this->webDriver->findElements(WebDriverBy::cssSelector('#ikkaktu_form div.block-body'));
            $this->_replaceItem($blocks, $inputs);
            $this->_addItem($blocks, $inputs);
            $this->_updateItem();

        } catch (Exception $e) {
            $this->fail("ItemUpdate: Not find element!\n");
        }

    }

    public function _selectItem(array $inputs = array())
    {
        $selectElement = $this->webDriver->findElement(WebDriverBy::id('input_select_fields[]'));
        $select        = new WebDriverSelect($selectElement);
        $select->deselectAll();

        $editElement = $this->webDriver->findElement(WebDriverBy::id('input_edit_fields[]'));
        $select      = new WebDriverSelect($editElement);
        $select->deselectAll();
        foreach ($inputs as $input) {
            $select->selectByVisibleText($input['text']);
        }

        $this->webDriver->findElement(WebDriverBy::id('jyuchu_dlg_open'))->click();
        $this->webDriver->findElement(WebDriverBy::id("ne_dlg_btn2_searchJyuchuIkkatuDlg"))->click();
    }

    public function _replaceItem(array $blocks, array $inputs)
    {
        foreach ($blocks as $index => $block) {
            if ($inputs[$index]['type'] == 'datetime') {
                $block->findElement(WebDriverBy::tagName('input'))->clear()->sendKeys($inputs[$index]['value']);
                $this->webDriver->findElement(WebDriverBy::cssSelector('#ui-datepicker-div a.ui-state-active'))->click();
                $block->findElement(WebDriverBy::tagName('button'))->sendKeys(WebDriverKeys::ENTER);
            } else {
                if ($inputs[$index]['type'] == 'text') {
                    $block->findElement(WebDriverBy::tagName('input'))->clear()->sendKeys($inputs[$index]['value']);
                } elseif ($inputs[$index]['type'] == 'textarea') {
                    $block->findElement(WebDriverBy::tagName('textarea'))->clear()->sendKeys($inputs[$index]['value']);
                } elseif ($inputs[$index]['type'] == 'checkbox') {
                    $block->findElement(WebDriverBy::tagName('input'))->click();
                } elseif ($inputs[$index]['type'] == 'select') {
                    $selectTag = $block->findElement(WebDriverBy::tagName('select'));
                    $select    = new WebDriverSelect($selectTag);
                    $select->selectByValue($inputs[$index]['value']);
                }

                $block->findElement(WebDriverBy::tagName('button'))->click();
            }

        }
    }

    public function _addItem(array $blocks, array $inputs)
    {
        $this->webDriver->findElement(WebDriverBy::cssSelector('#ikkaktu_form>div.form-actions>a.btn'))->click();
        foreach ($blocks as $index => $block) {
            if ($inputs[$index]['action_add'] === true) {
                if ($inputs[$index]['type'] == 'text') {
                    $block->findElement(WebDriverBy::tagName('input'))->clear()->sendKeys($inputs[$index]['value_add']);
                } elseif ($inputs[$index]['type'] == 'textarea') {
                    $block->findElement(WebDriverBy::tagName('textarea'))->clear()->sendKeys($inputs[$index]['value_add']);
                }

                $button = $block->findElements(WebDriverBy::tagName('button'));
                $button[1]->click();
            }
        }
    }

    public function _updateItem()
    {
        $this->webDriver->findElement(WebDriverBy::cssSelector('#update_form div.form-actions button'))->click();
        $this->webDriver->wait($this->timeWaitAlert)->until(
            WebDriverExpectedCondition::alertIsPresent()
        );
        $this->webDriver->switchTo()->alert()->accept();
    }

    public function tearDown()
    {
        if ($this->webDriver instanceof RemoteWebDriver && $this->webDriver->getCommandExecutor()) {
            try {
                $this->webDriver->quit();
            } catch (NoSuchWindowException $e) {
            }
        }
    }

    public function _login()
    {
        $this->webDriver->get($this->url);

        // Data login
        $params = LoginDataProvider::getLoginDataProvider();

        foreach ($params as $inputs) {
            foreach ($inputs as $input => $value) {
                $this->webDriver->findElement(WebDriverBy::id($input))->sendKeys($value);
            }
            $this->webDriver->getKeyboard()->pressKey(WebDriverKeys::ENTER);
        }
    }

    public function getItemDataProvider()
    {
        return array(
            array(
                array(
                    array('text' => '受注日', 'type' => 'datetime', 'value' => '2017/01/02', 'action_add' => false),
                    array('text' => '受注番号', 'type' => 'text', 'value' => '10', 'action_add' => true, 'value_add' => '_追加'),
                    array('text' => 'ピッキング指示内容', 'type' => 'textarea', 'value' => 'ﾋﾟｯｷﾝｸﾞ指示内容_更新', 'action_add' => true, 'value_add' => '_追加'),
                    array('text' => '納品書印刷指示日', 'type' => 'datetime', 'value' => '2017/01/02', 'action_add' => false),
                    array('text' => '出荷予定日', 'type' => 'datetime', 'value' => '2017/01/02', 'action_add' => false),
                    array('text' => '重要チェック', 'type' => 'checkbox', 'action_add' => false),
                    array('text' => '受注分類タグ', 'type' => 'text', 'value' => '受注分類タグ_更新', 'action_add' => true, 'value_add' => '_追加'),
                    array('text' => '作業用欄', 'type' => 'textarea', 'value' => '作業用欄_更新', 'action_add' => true, 'value_add' => '_追加'),
                ),
            ),
            array(
                array(
                    array('text' => '納品書特記事項', 'type' => 'textarea', 'value' => '納品書特記事項_更新', 'action_add' => true, 'value_add' => '_追加'),
                    array('text' => '受注キャンセル', 'type' => 'select', 'value' => '0', 'action_add' => false),
                    array('text' => '配達希望日', 'type' => 'datetime', 'value' => '2017/01/02', 'action_add' => false),
                    array('text' => '発送伝票備考欄', 'type' => 'text', 'value' => '発送伝票備考欄_更新', 'action_add' => true, 'value_add' => '_追加'),
                    array('text' => '発送伝票番号', 'type' => 'text', 'value' => '10', 'action_add' => true, 'value_add' => '_追加'),
                    array('text' => '備考', 'type' => 'textarea', 'value' => '備考_更新', 'action_add' => true, 'value_add' => '_追加'),
                    array('text' => '承認状況', 'type' => 'select', 'value' => '0', 'action_add' => false),
                    array('text' => '顧客区分', 'type' => 'select', 'value' => '0', 'action_add' => false),
                    array('text' => '発送方法', 'type' => 'select', 'value' => '10', 'action_add' => false),
                ),
            ),
        );
    }
}
