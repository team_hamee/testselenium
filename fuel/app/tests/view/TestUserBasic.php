<?php

/**
 * @group Hamee
 *
 */

use Facebook\WebDriver\Remote\DesiredCapabilities;
use Facebook\WebDriver\Remote\RemoteWebDriver;
use Facebook\WebDriver\WebDriverBy;
use Facebook\WebDriver\WebDriverElement;
use Facebook\WebDriver\WebDriverExpectedCondition;
use Facebook\WebDriver\WebDriverKeys;
use Facebook\WebDriver\WebDriverSelect;
use PHPUnit\Framework\TestCase;
use TestView\LoginDataProvider;

class TestUserBasic extends TestCase
{
    /**
     * @var RemoteWebDriver
     */
    protected $webDriver;

    /**
     * @var DesiredCapabilities
     */
    protected $capabilities;
    protected $url               = 'https://vm-ne-dev/User_basic_userset';
    protected $serverURL         = 'http://localhost:4444/wd/hub';
    protected $connectionTimeout = 60000;
    protected $requestTimeout    = 60000;
    protected $timeWaitAlert     = 20;

    protected function setUp()
    {

        $this->capabilities = DesiredCapabilities::chrome();
        $this->webDriver    = RemoteWebDriver::create(
            $this->serverURL,
            $this->capabilities,
            $this->connectionTimeout,
            $this->requestTimeout
        );

        $this->_login();
    }

    /**
     * @dataProvider getAddUserDataProvider
     */
    public function testAddUser(array $params = array())
    {

        $countUser = 0;

        $tagEdits = $this->webDriver->findElements(WebDriverBy::cssSelector("#main a[name=edit]"));
        if (!empty($tagEdits) && count($tagEdits) > 0) {
            $countUser = count($tagEdits);
        }

        try {
            $this->webDriver->findElement(WebDriverBy::cssSelector("button[name=add]"))->click();

            $this->_actionAddOrEditUser($params);

            $message = $this->_getMessageAlert();

            $this->assertEquals(
                $this->url,
                $this->webDriver->getCurrentURL()
            );

            $this->assertEquals(
                '追加されました',
                $message
            );

            $tagEdits = $this->webDriver->findElements(WebDriverBy::cssSelector("#main a[name=edit]"));
            $this->assertEquals(
                $countUser + 1,
                count($tagEdits)
            );
        } catch (Exception $e) {
            $this->fail("Add password user:Not find element!\n");
        }

    }

    /**
     * @dataProvider getEditUserDataProvider
     */
    public function testEditUser($index = 1, array $params = array())
    {
        try {
            $tagEdits = $this->webDriver->findElements(WebDriverBy::cssSelector("#main a[name=edit]"));
            if (!empty($tagEdits) && count($tagEdits) > 0) {
                $tagEdits[$index]->click();

                $this->_actionAddOrEditUser($params);

                $message = $this->_getMessageAlert();
                $this->assertEquals(
                    '更新されました。',
                    $message
                );
                $this->assertEquals(
                    $this->url,
                    $this->webDriver->getCurrentURL()
                );
            }

        } catch (Exception $e) {
            $this->fail("Edit password user:Not find element!\n");
        }

    }

    /**
     * @dataProvider getEditPasswordUserDataProvider
     */
    public function testEditPasswordUser($index = 1, array $params = array())
    {

        try {
            $tagEdits = $this->webDriver->findElements(WebDriverBy::cssSelector("#main a[name=edit]"));
            if (!empty($tagEdits) && count($tagEdits) > 0) {
                $tagEdits[$index]->click();
                $form = $this->webDriver->findElement(WebDriverBy::cssSelector("form[name=mainform]"));

                $this->webDriver->findElement(WebDriverBy::id('show-dialog-btn'))->click();

                $this->_enterInformationForm($form, $params);

                $form->findElement(WebDriverBy::id("ne_dlg_btn1_dlg"))->click();

                // Update user
                $form->findElement(WebDriverBy::cssSelector('button.btn'))->click();

                $message = $this->_getMessageAlert();
                $this->assertEquals(
                    '更新されました。',
                    $message
                );
                $this->assertEquals(
                    $this->url,
                    $this->webDriver->getCurrentURL()
                );
            }

        } catch (Exception $e) {
            $this->fail("Edit password user:Not find element!\n");
        }

    }

    /**
     * @dataProvider getDeleteUserDataProvider
     */
    public function testDeleteUser($index = 0)
    {

        try {
            $tagDeletes = $this->webDriver->findElements(WebDriverBy::cssSelector("#main a.btn-danger"));
            if (!empty($tagDeletes) && count($tagDeletes) > 0) {
                $tagDeletes[$index]->click();
                $this->webDriver->wait($this->timeWaitAlert)->until(
                    WebDriverExpectedCondition::alertIsPresent()
                );
                $this->webDriver->switchTo()->alert()->accept();

                $message = $this->_getMessageAlert();
                $this->assertEquals(
                    '削除されました',
                    $message
                );
                $this->assertEquals(
                    $this->url,
                    $this->webDriver->getCurrentURL()
                );
            } else {
                echo "Delete user: Don't have user to delete!\n";
            }
        } catch (Exception $e) {
            $this->fail("Delete user: Not find element!\n");
        }
    }

    public function tearDown()
    {
        if ($this->webDriver instanceof RemoteWebDriver && $this->webDriver->getCommandExecutor()) {
            try {
                $this->webDriver->quit();
            } catch (NoSuchWindowException $e) {
            }
        }
    }

    public function _login()
    {
        $this->webDriver->get($this->url);

        // Data login
        $params = LoginDataProvider::getLoginDataProvider();

        foreach ($params as $inputs) {
            foreach ($inputs as $input => $value) {
                $this->webDriver->findElement(WebDriverBy::id($input))->sendKeys($value);
            }
            $this->webDriver->getKeyboard()->pressKey(WebDriverKeys::ENTER);
        }
    }

    public function _actionAddOrEditUser(array $params)
    {
        $form = $this->webDriver->findElement(WebDriverBy::cssSelector("form[name=mainform]"));

        $this->_enterInformationForm($form, $params);

        $form->findElement(WebDriverBy::cssSelector('button.btn'))->click();
    }

    public function _enterInformationForm(WebDriverElement $form, array $params)
    {
        foreach ($params as $param) {
            if ($param['type'] === 'text') {
                $form->findElement(WebDriverBy::name($param['name']))->clear()->sendKeys($param['value']);
            } elseif ($param['type'] === 'select') {
                $originalElement = $form->findElement(WebDriverBy::cssSelector('select[name="' . $param['name'] . '"]'));
                $select          = new WebDriverSelect($originalElement);
                $select->selectByValue($param['value']);
            } elseif ($param['type'] === 'multiselect') {
                $originalElement = $form->findElement(WebDriverBy::cssSelector('select[name="' . $param['name'] . '"]'));
                $select          = new WebDriverSelect($originalElement);
                $select->deselectAll();
                foreach ($param['value'] as $value) {
                    $select->selectByValue($value);
                }
            }

        }
    }

    public function _getMessageAlert()
    {
        $this->webDriver->wait($this->timeWaitAlert)->until(
            WebDriverExpectedCondition::alertIsPresent()
        );
        $alert   = $this->webDriver->switchTo()->alert();
        $message = $alert->getText();
        $alert->accept();

        return $message;
    }

    public function getAddUserDataProvider()
    {
        $id = time();
        return array(
            array(
                array(
                    array('name' => 'user_code', 'type' => 'text', 'value' => 'staff_user_' . $id),
                    array('name' => 'password', 'type' => 'text', 'value' => 'ADD1234'),
                    array('name' => 'password2', 'type' => 'text', 'value' => 'ADD1234'),
                    array('name' => 'tantou_name', 'type' => 'text', 'value' => '山田太郎'),
                    array('name' => 'mail_adr', 'type' => 'text', 'value' => 'test_add_mail@hamee.co.jp'),
                    array('name' => 'tantou_kana', 'type' => 'text', 'value' => 'てすとぶしょ'),
                    array('name' => 'busyo_name', 'type' => 'text', 'value' => 'テスト部署'),
                    array('name' => 'syakai_hoken_kbn', 'type' => 'select', 'value' => '0'),
                    array('name' => 'seinengapi', 'type' => 'text', 'value' => '12/12/2000'),
                    array('name' => 'yubin_bangou', 'type' => 'text', 'value' => '123-456'),
                    array('name' => 'jyusyo1', 'type' => 'text', 'value' => '神奈川県小田原市栄町2-9-39'),
                    array('name' => 'jyusyo2', 'type' => 'text', 'value' => '小田原EPO 5F'),
                    array('name' => 'keitai_bangou', 'type' => 'text', 'value' => '090-1234-567'),
                    array('name' => 'denwa', 'type' => 'text', 'value' => '0120-12-3456'),
                    array('name' => 'nyusya_bi', 'type' => 'text', 'value' => '2/2/2015'),
                    array('name' => 'taisyoku_bi', 'type' => 'text', 'value' => '3/3/2020'),
                    array('name' => 'tantou_tenpo[]', 'type' => 'multiselect', 'value' => array('2')),
                ),
            ),
        );
    }

    public function getEditUserDataProvider()
    {
        $id = time();
        return array(
            array(
                1,
                array(
                    array('name' => 'user_code', 'type' => 'text', 'value' => 'staff_user_edit_' . $id),
                    array('name' => 'tantou_name', 'type' => 'text', 'value' => '山田太郎'),
                    array('name' => 'mail_adr', 'type' => 'text', 'value' => 'test_edit_mail@hamee.co.jp'),
                    array('name' => 'tantou_kana', 'type' => 'text', 'value' => 'てすとぶしょ'),
                    array('name' => 'busyo_name', 'type' => 'text', 'value' => 'テスト部署'),
                    array('name' => 'syakai_hoken_kbn', 'type' => 'select', 'value' => '0'),
                    array('name' => 'seinengapi', 'type' => 'text', 'value' => '12/12/2000'),
                    array('name' => 'yubin_bangou', 'type' => 'text', 'value' => '123-456'),
                    array('name' => 'jyusyo1', 'type' => 'text', 'value' => '神奈川県小田原市栄町2-9-39'),
                    array('name' => 'jyusyo2', 'type' => 'text', 'value' => '小田原EPO 5F'),
                    array('name' => 'keitai_bangou', 'type' => 'text', 'value' => '090-1234-567'),
                    array('name' => 'denwa', 'type' => 'text', 'value' => '0120-12-3456'),
                    array('name' => 'nyusya_bi', 'type' => 'text', 'value' => '2/2/2015'),
                    array('name' => 'taisyoku_bi', 'type' => 'text', 'value' => '3/3/2020'),
                    array('name' => 'tantou_tenpo[]', 'type' => 'multiselect', 'value' => array('2')),
                ),
            ),
        );
    }

    public function getEditPasswordUserDataProvider()
    {
        return array(
            array(
                1,
                array(
                    array('name' => 'password', 'type' => 'text', 'value' => 'EDIT1234'),
                    array('name' => 'password2', 'type' => 'text', 'value' => 'EDIT1234'),
                ),
            ),
        );
    }

    public function getDeleteUserDataProvider()
    {
        return array(
            array(
                1,
            ),
        );
    }
}
